﻿using System.Web;
using System.Web.Mvc;

namespace A4_BigSchool_5_ConBao
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
