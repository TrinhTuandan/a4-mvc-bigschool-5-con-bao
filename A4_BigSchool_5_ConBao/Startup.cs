﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A4_BigSchool_5_ConBao.Startup))]
namespace A4_BigSchool_5_ConBao
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
