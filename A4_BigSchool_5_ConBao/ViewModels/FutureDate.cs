﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace A4_BigSchool_5_ConBao.ViewModels
{
    public class FutureDate : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime dateTime;
            var isValid = DateTime.TryParseExact(Convert.ToString(value),
                    "dd/M/yyyy",
                    CultureInfo.CurrentCulture,
                    DateTimeStyles.None,
                    out dateTime);
            
            return IsValid(isValid && dateTime > DateTime.Now);
        }
    }
}