﻿// <auto-generated />
namespace A4_BigSchool_5_ConBao.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class Dbchange : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Dbchange));
        
        string IMigrationMetadata.Id
        {
            get { return "202403190200094_Dbchange"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
