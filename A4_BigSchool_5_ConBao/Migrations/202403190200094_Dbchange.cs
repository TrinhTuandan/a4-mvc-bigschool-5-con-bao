﻿namespace A4_BigSchool_5_ConBao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dbchange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "Place", c => c.String(nullable: false, maxLength: 255));
            DropColumn("dbo.Courses", "LPlace");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "LPlace", c => c.String(nullable: false, maxLength: 255));
            DropColumn("dbo.Courses", "Place");
        }
    }
}
